// console.log("Hello")




let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);

let currentIndex = users.length - 1;

/*
    1. Create a function which is able to receive a single argument and add the input at the end of the users array.
        -function should be able to receive a single argument.
        -add the input data at the end of the array.
        -The function should not be able to return data.
        -invoke and add an argument to be passed in the function.
        -log the users array in the console.

*/


            function newUser(user2){
                    users[users.length++];
                    users[users.length-1] = user2;
                };

                newUser(prompt("Add an Element to the Array: "));
                console.log(users);

/*
    2. Create function which is able to receive an index number as a single argument return the item accessed by its index.
        -function should be able to receive a single argument.
        -return the item accessed by the index.
        -Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
        -log the itemFound variable in the console.

*/

        let itemFound;

                function getIndex(num1) {
                    if (num1 > currentIndex) {
                        while (num1 > currentIndex){

                            alert("You can't get an Element that is not in the Arrays!");
                            num1 = getIndex(prompt("Input a Number in the Range of the Arrays Index to get the Element. \nThe Current Arrays Index length is: 0 - " + currentIndex));
                        }
                    }
                    return itemFound = num1;
                }

    getIndex(parseInt(prompt("Input a Number in the Range of the Arrays Index to get the Element. \nThe Current Arrays Index length is: 0 - " + currentIndex)));
    console.log(users[itemFound]);


/*
    3. Create function which is able to delete the last item in the array and return the deleted item.
        -Create a function scoped variable to store the last item in the users array.
        -Shorten the length of the array by at least 1 to delete the last item.
        -return the last item in the array which was stored in the variable.

*/

    let userDel;

                function removeElement() {
                    userDel = users[users.length-1];
                    users.length--;
                    return console.log(userDel);
                }
    removeElement();